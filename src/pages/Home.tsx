import {
  IonCard,
  IonCardContent,
  IonContent,
  IonHeader,
  IonImg,
  IonItem,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from "@ionic/react";

import "./Home.css";
import { MouseEventHandler, useState } from "react";
import { AnimatePresence, motion } from "framer-motion";

interface Post {
  id: string;
  image: string;
  name: string;
}

const Home: React.FC = (): JSX.Element => {
  const [selectedPost, setSelectedPost] = useState<Post | undefined>(undefined);

  const posts: Post[] = [
    {
      id: "1",
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9UJmlALF5PQd0xPTQvj1ZfR9OHPTGAZtTFg&usqp=CAU",
      name: "AC Milan",
    },
    {
      id: "2",
      image:
        "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFRUVGBcYFRcVFxUVFRcVFRUXFxcVFRcYHSggGBolGxcVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lICUtLS0tLS0tLS0tLS0tKy8tLS0tLS0tLTctLy01LS0vLS0tLS0vLTUtLS0tLS01LS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAACAgMBAAAAAAAAAAAAAAABAgADBQYHBP/EAEsQAAEDAQQECAcNBgYDAAAAAAEAAgMRBBIhMQUGE0EiUWFxgZGhsQcyUnLB0fAUIyQzNEJic4KSssLSFVNUk6LhQ0Rjo+LxF4O0/8QAGwEAAQUBAQAAAAAAAAAAAAAAAgABAwQFBgf/xAA+EQABAwIDBQQGCAYCAwAAAAABAAIRAwQSITEFQVFhcSKBkaEGEzKxwdEUNFJicqLS8DVCgrLC4SMkFSWz/9oADAMBAAIRAxEAPwDlu9RTf1ooF06DfSUSFAockk4GSr/ukIVlM0aJKPCqCFZGErgrAkgaM1YFCixEJlbGilESjVK7iSRJQEKJyoQkhwquiIRARCZCAoSigEwSUjUHIAInNEhJKJVTgoGpzmoE6DCEjmqFqYZoEJIcISoqIpJBRJRMgMk6YpaKKVURKKAn3oqb0UKlCgGPWg7JH+6hTIkpTUQLUUkgFXTu9KDU4GSICSjDUwTtCX27U5CSsNUQA7EXoNGaZFvhQoFPRK71d6SRQaECKKyiVySREBK1MEdyDUkgFGlGiDBgiUk40VYClaI0wUfkkgOQQag4JykKSYhCiiJCidDCCSqeiSicIHSkUTURU2MKDAU/901EB7dSKgVkBTehxIgIgJ04ChCieiqmdu6uRICcglVe2kwvcYAUamAXsOh7U1oebO8tcKggVNDkSG1LekLyPddwLHA8RqPQmaA/2CD0IKpt2hQAGOW9Wn4SFKYu5vS5WKuN1cmOPNj3Be2zaLtUvxdnkPLdoPvOoEzxgEvIA5kBSN2lbR2ST0a4/BeaTAY4BVtcCtw0RqC8m9aX0HkMNSeQuyHRXnXs0xqHG8X7K64fJJLmGnLm09az3bVsmvwY/wCoA4R36nqBHNRm7uCcQp9ngT2j8B0laKECMfsr3W3RNqs5pLC4jygKt57zcOteBszePj9sFoMGMYmQ4cQZHiFM3aFucnuwng7L35eBKsQOY9tyBnZxq6x2OaY0hic/6W7ryCdzS0S7IcTkPEo339sNHgng3tHwEqt7gOZVkg0piLy3fQmomN+1uB33GnD7TvQE2l9QQavsr6VxuONR9lwyWd/5Wz9Z6vH/AFQcM8J174jmoTd1icXq+zwntdeHctKCDsl77XoS1xePZ3c7Re7l4pWuGbHDnBCvsIeJYQehB9ymF/QjMkdQR+/FK5KQrY2F2AY48wJ9CstGi52M2roXNZxkUz7U5hphxAJ4kZ9ENS+oASHT0BMc9F5qJSFaMeZVnNJWjBEhAhSiYpU6EhKlTpRknQOCFUEUEoUfenHt91FBqKSkCgCdqVqZqZG0J0LDM2Kdj5GX23rxHGB6RgaZGgRCWVlcswKt872wSyOR0OR6FQ31ua9KG6jMdRx66LrdptwbDt2DaNoHYGlWHEuGGNBjTkXq2dq/hJXA5FklmLTyisoNOhan4ONJX43Wd2NzhMB8hx4Q6HH+pdB1NlIifZ3Z2d5jbyxEB8PU11yvHGVztpsu3dXqWtcS5sEGYluXdvHiRuWRVu6nq21aehyI4H9z5LHRxWo/5KUc8lmA7JSqdLttkML5dnC1rBjelLnZilGtZTf5S3eiwevHyKbzR+Jq0HbDsqTHODJIBOZO4coQ2t3Vq3FOm45F7QeYLgCuVW7SM82EkpIObI6xRnnDTfPMSQrdAB4njZE90V9zQQ34vFwFTGeD1UPKvCFkNXflMX1jfxqtTa0QyBhkSIyPcvQX2VBtFzQwaHmchlmc8t2eS6I/RtqZmxkw44zccfsSGg++VjrVYIz8bYnV5YNp2sDh2regnaKq7U2DaF0txN/CfmCvNmX9YNgweo+ULmTTYmNMjbKQ0Y1FleMByli9OjNKm0FzbPDS5Spkc1rca0oGVJy5FmtZLCIrI8N8W47Pjrj15rWPB/nNzDvK5e5t/VitjklhgSZykQe8FdJaUhVsnXE5iNNN0+8rH6wSTC0COaW80FhuNFyM1oTUVJcOc9C2PT1nEEJmhNxwLcB4hqd7Thv3UWva5YWvoYexbXrWK2STmHeENRw/653GJG4+zqP3zV6rb04tuz7UTznDqsboDSE87C7Zxm7wTQlhOANaEelB2m27bYOgdfvXcNmRWtM6pvB8fepB9MfhWJteGkx9cztcPWh9VTdXqswiGgkRyhJtlTdc1aeYDRI8uMrYnuc0E7AgDEklg9K82krTGLM6SZtG3auaaE4jLnWU0yahkflux8xuJ9A6VoPhKt+MdnafpnuaO8ptn2/0msynESZJE5NGZ3+HOFhVKpawu1OgHEnQea0uDeQKAnBvFyInNOBTmCr3ru3GTK0aFH1NJtOZj9+HBByCYhKkjKhSbkxSEpwM1G8quqiF1RWcCp4irgmCACIVZXQi0YJmpW7lYmRtTBAZ9CigOPWEyklZHU+07K2x40D3Fh5RIMB9+nUugaQ00+xWkPYxrttDdcHAkVhfVpw30leuY2A/C4T9OE9O0XQNeRjB58o6Nk4+gLHv3ervaNRuRLHDwn5rKtaNN1y+i8S31gyP3o+crIf+RZf3Mf8AX+peLTOuslohdCYmtDxQltcKEHe7kWqhBSuuapEF3uXVs2NY03h7aQkEEZnUZjeor7DaTFIyQYlhBFciQQcepUIhQLTgEQVu58I037mPqf8AqW3au6Zlki2sjGtvE3QA4cAYXjU7zXoAXKdCWPazNYTRpxeeJoz6cgOUhbtrRpcRwiOOg2gutA+bGMHDuaOd/EtCjXqQalR2Q8yuZvdlWuNlChSAcTmc8h49/QLaJnR2uxvbG6817XAEcdcxVaPqRA6OadjxRwABHKHBZfVG2bOyMc48Gjw/kaJHcLo9azM+jwJDOz5zLrqb8Rdd206lj3j/AKXaVqgHaa4h3QOn3fFZlrV+j0alA+y7TqIy7/lxXPtdvlQ81vcVtWsONjk8xve1atruPhI81v5ltWmRWxPP+kD3LHqGKdsf3q1bNX2LQ9P8VifB6eBKOVvcVi9J4aS/9je9qyHg7Pxw5Wdz/UvBp0U0h9uPuYpx9dqj7vwarNMf+wq/h/StrtZraB9CP8b/APiuS61zl9umO5hujmaBXtqutTD4TJ9XH+ORcd0xX3VaK/vH/iKtejoHrHngweZErlYmpSH3vcCfgvKkCdIzfzrqVsu1CDgoi5BJBCCqoriqk6jcElFE15RHjUGEcVYAgAiFAgU4TAIj261ER7daRRhFSLL7Tu9RFhABPFeTKUar1avQ7S2wt/1GHojAee4rpektDutdoigaQ0Njllc4guHjRsaOc3nfdK0/wZ2IunfMRhG3D6yQ8fI0HrC6fqnFemtM+NKsgbxERAveR9qUtP1ay6rRX2o2nupsz6mf1N81zYunU2OuGHNz5b3HL3FYL/xu79+PuLxaa1INngfMZr1wA3Q0itXNGdeXsXT1gdeR8Bm5m/jatKraUm03EDQE6ncFbs9u39S4psfUkFzQey3QuAO5cgV1hg2kjGVpeIFaZVIFadKpC92hCBaIiRUB7cMqmop29yxxmYXfVCQ0kcPNb3YtQjGeDaMSQTVnk4tHj5A4/wDSa26hbV4cbS6gpdFwGgGAGLsTvJ3kkrdB3qBbptKJGGMup+a80G3b+Q/1mfHCz9K0Wzw3LBKytbotDa5VuSyNOHQsn4O9K7eN0MmLomkY/OZhd6sugca8TR8FtPnW3/6J1hfB1aCyd5H7s4cYvsqO1claXAtq1ySOyHkHpiI8lu0KP0jZj3HUQ4dYHzKp8IVnLLUGnyG0PGKmh9uJbJpX5E76pvcF6dftEi0RCWPF8bbw4yweMPT0Lz6Y+Qv+pZ3MUO1LdtF1FjfZnLpIU9K4FehbcWug9clg/B5lN9j86x2sny/pZ3BZLwef432O96xms3y487O5qan9fqfh+DVqU/4jU/CP8VuNtFLQ36cZHSx4P5yuSa2WfZ26UbnkOHM4AntquvabFAyTyJBXzH8A9AvA9C594TrFR8VpAzqx3OKub2XkXo9VDa7W/baW94zHuA6lchWdga2p9hwPdofIlaekG9OEg3rr1unco5BF6CSA6oFV7laVWMk6BwQqokURqGVafV3pgEisQKVqim7240CiAkiRdl1fiSWk43R87izpuHOmkfQV37uVbXqBq+ZH+6pRwGn3sH5z/L5hu5eZQ167Leka1TQbuJ3AfHlnuVK+uCf+vTPacMz9lu89+5bbqvok2azNZhtDw3+e4ZdAoOhZLRNotVnibE0Wc3akuIkBc9xLnvNDm5xcelXqALjKW07mk99Rju08yTAPHiDGp8uCgfbU3ta0jIaZqz9sWzybP/urx6XtFsnifF8HbfABN2UkUIOHC5F6UVMdt3pEF/5W/JDTtKTHB7RBBBGZ1GY3rR5NU7QASZYAB9CT9a8mirIzaB/u6xXYzffQ5NDm79phUlrRyuC3fSLqscPonuK5VoaWxQQ3LVFM6R917tmQ0Bt3gMILhkDe53U3LQ2Ya1yHPJPZIya1pJmdxjSM1YutrXLIb6zWdSAP7ea6zZNP2uYF0VosLwDQ3IpXgHOhItHFRS26ctkTb8lpsUbeN0EoHIBW04lU6G0RFZmlsLSA43jVxdjQDfyALULbZ/d2lHQyk7OFpo0EioAZUV3EudiRuogobQua9V5FYim0FxOFmKBG4CJJIGvyWdUt6bGDsDETES6J6ys5YJppmSRw22yPDjK592B7nDbve52G3wFXuAw3b1XYdET2Mul91QtAaQ4vs5oG1BJwlHEFfo3VKCCcTxF7aCgZeJbiKGpNSRlgTuXq1u+RT+YfQs+pVY+vgpOJbUIxS1gMk5zkZIJmRCu0n1aVBzTlloCYMDLnuXs1Z1gike6J9sgkLhVgYwspQEuFC81wx6Cq7bZpZQ+Jk8YiODfeakMpgA6/Q046Lmuj9IRRQNc2xv27KubPeeGh4cS1xAwIGAIyNCDmt91UI2DXtNWyOfI0eQHvJMfO01B5QVr7VoGjaMAnsPwjE1hkQYIiYiN4BzGWWdG0rl1bqA7IuyIjj13eYKOidCT2cuMdoj4VK3oa5VpSkg4yq7bq3LLKZX2ht6oNBFRuAAGBceLjWx1UXNi9rB5qSMR1OFvyWwKtQPLw44jqZM7vkFj5YLQ9rmOmYQ4EGkVMCKeWvPrBon3RZXQk1ddF0ny25Hr71mErlGys9jg5kAgyIAGY6QofVNIIOhELhcYIBa4ULDRwOYpmgN62rwh6E2b/AHTGOC/4ym53lcx7+daqHVxG9eg0LhlxSbWZodRwO8fLlBUmz6rsPqX6s38W7j8ClfkgjJklKlV06qFV7laVWU6icq8VEt5RGq2JWlWqqisaUCsNRKiiISUgVMjaOrSozxyw3cy65qtpmK0wi4AxzAA6MfNplT6PEVytgWX8H4ItoAJA98rygA4HkrTqWdtW2bcW5c4wWAuB94I58dxWXXo/R6oqM0eYI57iD4yDlourhGi1DXDSk8czGRSmMXLxuhhqS6g8Zp4j1rB/t21/xL/uw/oWRZ+jN5d0G16bmQ6YkmciRnDTw4qjcbXoUKhpvDpHADhPFdLTBcz/AGxayD8KkwPkxcR+hyKsaetX8TL1Q/oQ1/Ry6ouwPcyeRd+latgDe0fXUhlJGeRy5LolpwBwBoDgcb3Otf1G0eWxyGWJwL5L7dqyjiHMacajMGoNMKgrWDpe1OFfdMvVHuFdzV6YdLWk1jNokr4zCC3HCpaKN3jEcop84rQs9g3dW3qUmOZBLSZLt0kfy8zPcqG06rbCqw1gZIdEQcsp1I5LpLVpesei7TBavdtlZtKjhsAqa0DTwRiQQBliCFhP2taf4iX749SM+k7QD8om+/ymm7kSb6P3mz3Yy5hDgWkHEQQdQchwnuSsqrNql1OkCC2HSYEZwI189y2PV62aRntG0lZsYAKFjmkVONLt7hXqnPKgyWb1ls7pLLMxgLnOYQ0DMnkXOzpOc/5if+YR3J/dc1BS0T/z5DhRpzBQM2JXublpperbhggAOjsmczmSTvJKs31F2z7U1axLhMZGTLuAhoA9yyFhm0pHZxZmWQXbrmhzga0cSTm+7vOeC2vV7RbrLZ2QP8dtXOFa0e+hLejAYYVBWq6CM73l+3nozeZZC28cqgmmGLscOCBvWPtksrXOO3tFQRX3+bC8C4DB4xoO3kWjfbDuhbPP/G2XBzsOOXGd8zvM7gFi7MvKV1d07eniLiCG4ogAAk6cmnce5dXY2gojRci/aE37+f8AnzfrVkdtmoSLRPUV/wAeY5U+nylYFL0brVagYKjZPVdPe277S3dXfBDRJgme6QPeusJXLlPu6b9/P/Nk/Uth1Ktcjp3tfI54MZNHvc+hDmiovHDxlLfeitzZ27q76jSGxkJnMgbwOKwbbbdG4qik1rgTxjcCePJWa+adZDGYRR0kjSKHENafnO9AXOImUCyuuIpbrT0H/aasaVsbOtmULZmDVwDieony08ytiymoXPO4loHCDn4pXIKHJQK8rqCrOSsVYyCSicq6qKIKVVpV6kWTUh3JmqJWBqrSoEEapKVMFl9Q3gW5vLfH9FVhwvToGcR26J26+3+sXSobhpfRqMG9jh5FUto5Mpu4Pb5yPitu17b8IYeOPuc71rXVt2vsGEMnEXMP2hUdrO1aitT0XqipsynG7EPzE+4hcRtphbeOJ3wfKPgmbkef8rlQrx4p5/yuVKDav1juHvK7v0S/ho/E74Jo8+g+lEDeDQjEHmxw5Rn1qRZ9B9CJH9lb2P7D+o+KxfTTKrQ6O97VfagKCTANdUncGuFL7eQYgjkcF626ImljdaA1rIgxz78rnC8wXn1bGBUih30WzaI1ajn0e6TAuc12xrk2UBzG3xxtc5w4sQdwWdtzmR2d5LbzGRngjNzQ2lznOSw/Sba76T2UKTZz15iJA5iRmclW9HW1aLX1mmC4aRunI56THVaQNTbVsw69CXEAlhvMpUVpexqegLFTQOa7ZvY5jx80gXiKBoLaYOHKKroFj0XaGsbW1vvBoF25E6MUGWLQ91OO/UrIaL0W60SN90xMrA4Pjlj8R8mNKA8JmVS01GLaE7srY+2azLsAhrwZGUtIH2hIEiBzPPctPapq3NoaT3HccwDmN08/Dgsa2xNsViJeASBVw8qR2TOatB5rKrnkry684mpLqk8ZNSStu8IWldpKIG+LFi7lkI9Aw6XLT3b+cdzl295Js3E78P8Ae1c/6OBo2xSA+/8A/N6rV0Piu6fyqheiHxXc5/Ksew+ss6/Begbf/hlf8PxCVbHqIz4Q88UVPvPb+la4tt1Bh+Ok8xnS0Oeex7Va9KKgZsurO/CPzD5LzLYjS69Zyk+RHxWl64OrbbT0jqjA9C8BVulZ9pPaJPKe6nMXGnYAqnrNpNLKbGHUNaPABd1szOi53Fzj+Yj4Kt+SCLkEYVw6qKrcFYCq9ycKNyrpyqIqKaFVVicKsJ1CrLU1UQkRakjCdVTYEOGBbwa9Rr1g9asBUc2oI9qpwYIKjuaXrqTqfEZddR5rqkvw2wgt8ZzA4ckjMadYotCaa4/9rL+DfTV1xszzQOJLK7nfOb059au1s0TsZdo0e9yGuGTXnNp5Dnz15FV2DWFje1LF57LziYePLvAjq2OC5Pa1A3FBtw0ZtycPf4HyKwm7p/K5Uq7d0+gqpau1vrA/CPeV1Hoj/Dv63fBNHn0epEe3WgzPo9SLVb2Mew/qPisX03H/ACUOj/e1dN1CfSyNZueZCPrRM/A89AOgLN6wWJj2OxoyZrgSMC0kcLHcd/ODxLnuqdqkdesombGDV7TcvScLF4jcXXQQeFi0+MeJdRFhitUF17GnygRUX24gkb8celcdd0HVK1e0qnthxezjBOIaxuy1y03Qlb1B6qnWYOyWgHuEfP8A2tP0dNbZmNew2cg1DWlj/fg00Dw+972H0qOC6gIzW12bSDBDwczgAfGvHMuG4jE9AC8kJc00DCX4hoIN1tMC5x6aAZnHiqMFrRHDHZ3PdS9HXYyNDRIZj89p8pzs9xGeFVm280KeMDDUeYaABInSRrMwdDMCQBkrb4cYObRrw/evRc7tAcJJQ919wllvOw4R2jquNN5NSeUqo+KfOHcU945uNSa3jxucauPWSkOR84dxXpF0xzNn4HagMB6hzQVz/o88P21Te3QmoR303lIr4RwXdHoVCvi8V3R6FjWP1lh5r0Lb38Mr/hKVzgBU5DNbqCbFo5znYSFpdjntJcGtPNVo+ysHqxor3RLVw96iIL+Jzhi2PuJ5KD5yq8I2l9pI2zMPxZvP8+mA6AT0u5FX27XF7eU7BmbWHHU7v5fAmfxDSCuA2TQdQoOuI7TuywcZO7qY8CtQY3gHzT1Yg+3KncjJlTjFOxLVSk4iSuzt6QoUm0huAHzSlAJkoOSSIoKvcnSFIaqJ6TBRRRTwq6sCKRqZQqduiYlMFWSrKplIDmiUQlfknSRjVI8lrr7SQRQ1GYIycF0/VrTsVuhMUoBfSj2nJw8pvtguaBCJz4niSMlpBqCM+j1Kpe2bLunhcYcM2u4H5Hy1WfcW7mPNakJn2m8eY5+9bdpzQb7Ma4uiNaP3tF0ij/WsIRTNbdq7rvHMBHaKMflePiO5+I8hXs0jqhE/hQO2ZON0cKI9G7oVB+1a7HinfiHAQHbnDPMxrrqJ5gKxsi5oW9Ispjskk5biYkQemm7dOg0Zvt1Jm+tZO26AtER4UTnDyo/fB2cPsWMJxoSAeImh6ius2FWp1Kb8DgcxoVhemVRtV1BzDIAfPLNsTwnmrGkgggkEGrSDQgjeDxrctTNZXRS7O0F0m0N3abQ0wc4NrGeCMTSozrjktLTTg3iaHJ3eVJtmiyKdQt7QOR0Ohyngd405Kr6J2zbt9ai4mMIOW4zExous61azss8dWxlznYMF5rQairnVxIAFBUA4uC5ppXSMtoo+VzSMbjG4MY0gHgje6h8c9iXSmkJJy0vqaAN6cSXc+LiefkC8zhwRz/lCz9mW9J1963D2gDBOZ6jgeY6LX2/YCz2USSZLmg8IJ0gZcJmeSRIcj5ze4qbRtaVbU5AGpPMBiVkrHoO0yghsL2ioN55MTcK1w8Y9AWvtavSpWzvWPDZiJIE9ocVzPo1LNo0qpBwjFJgkCabhuneViwK5LLaD0RLaCWtwjrwpaYUFMGV8d/YN/Edg0fqhGzh2h+0piWgBsX2vnO6TTkXk1g12jiGysga5wwDgPemj6NPGPZz5LiG7Sq1amCxBLvtaBvPP4iOAcu72ntGk+kabh2DkZ1PIDn4xuGq9esOmYtHwCCEDaU4Dc7oOcj+M1qccz0rmza4uJq51SScTjjUnjKMsjnSF73FznElxzJ56e2CCv2dm21ploMuObncT37hz1zO9UrS3e54rVREey3hlqee6N3VK45e25QpZPm+3zSira0Z1SlAblHKApKM6qJCignQOSUURUUsqHCgxOq2lWKIo2HJQZpkoTJlIEScE1Um/rTBOUTSnqhHuQUjyCZHOf75KSRg4jrWQ0Tp61Wb4t5u+S7FnVmOheElMhe1r24HgEcCJCr1bKnVdjzDuIyP+++Vu9g8IrcBNCRysII6QcQsxHrRo+YcJ7DySsp+IUXL6cYBVYiGKyquxLJ5kAtPIz/dJ81B9HuW+y8OHMQfEZeS6s2HRb8R7lx8ksb3FetmrNky2Ix+k71rkBgG6nSVtGqWt7oKQz1MYwBzdH+pnd2Ktc7KuWU5t6z3R/LJHhnmeWU7p0UDi6i4GswAHLEMxPA5AifDit6GrNj/cM7fWvJa7Bo2AjaR2ZhOIDw0kgbwChrBrZDZ4w5jhI94rG1pBBByc47mrl9utUk73SyuLnO38Q3ADiHEquz7C5uml9Wo9rPxGXcgCdOJPdKdz8T/V0mgu38AOJMb9w1PTNdJfrXo+HCNzeaKM06wAO1Ye3+ETdBCT9KU/lbn1haQxg3d5KenOAtWlsWyYcRaXH7x/TE98qQWt0/2nho+6CT+b5L1aT0vaLT8dIaZ3Rg0Y+SM+c1Xja0DL01U+d9n0qErUbDW4GgBvACArFGypUTjzLvtOzP8AruARJy9tyBQriPbcgU6syg70olK5EpIJSkpQckUBuSQE5opfWpVAJ0JKCiiCOFHKgyVir3J3IDqnbooEQlRZ60yIFMEWlCqgSUgTgohJVQFJOCnTFV1RqkjBT1Qac0tVGpJw7NWApZm159ylVK4pJPAc0tdmCkbHx9itSkoEp3OJMlR0aNOg3DTEBM0oEoNOSBKFSYslK4qEoN9aKdBMoE5e25SqFcVEkwKJ9upKifUgUkMoKBAlQJ0M5qIIpQkhKCiiiNDKG5OUBklBwQFM05JkzUhKYJIgc0xKgQJTJkYKigKBKAKdLFmmCYJAVGlMiDkyIKlUoKSKUylcUoKIKSUp0p3IVQJySTkplKpSVCUkJMIg+nvUJShSqSaUONFAb/bcjVOgBUqoVAgUk8oORSohJDKKVFKN6SYoIoKI5QKNUCKiAph+/JBFqiiSJqc+tEKKJgjSyeK7p7ioVFE6SIUj/MoomRDVP60AookE6CLfSookEkUPWookiKjUrvV3qKJIHeyndvSqKJ07tUo9Xciook1RjRT+yiiiScJf+KZRRJMlCnGgokmQUUUU6jX/2Q==",
      name: "Barcelona",
    },
    {
      id: "3",
      image:
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABAlBMVEUAQXD////jBhMAP28APW4AOmwAN2oAK2QAM2gAKWMANWkAL2bjAAAAMWcALWUAJ2LOq13jAAjYCx4AJGHaO0Tu8fQ6X4X4+vvV3OPc4ujw8/bj6O3Q19+YqLrDzdeHmq+otsVDZYiMnrK5xNBrg54AOnFwh6FeeZajscEAN3F6j6csVn5ScJAcTXjWsFy9x9JLa4xYdJPz4uSzm2DJqV15d2gAMnKCfWcVSXYmUnzx0tTml5vptLfhZ2zic3jfMjrgWF7ifYK/ol+WiWRBWW1rb2mlkmLetVruqavbQ0vzzM3v3eDeU1rfISruwsXmi48AGFteaGpPYGwwUW4ABlYAAFWKUm44AAAgAElEQVR4nL1dC3vbNrIlC74ZUnFViXpRb0uyZEdykjZym2xfu2263T733v//Vy6AGZAgCZKQ7dzZ79vdWBKJAwxmzgwGgGF+chkNp6t0udne9g93Z8MwzueH/mm7maXz6XD06V9vfMqHD4/puu/4ceg5jm1ZhBhciEEsy3aCIIr96LBdroafshGfCuF4te97buTYiKpOCLEDLw4P+/mngvkpEI7na9v1HKsZWxGn47nhdvcpUD47wun+7Hu2PjhJbM8lm2PyzA16VoTJahuGToteNg+mHbmn+bOCfEaEx20napt2OmJ53dPq+UA+F8Lh3gnrdJPYjuNFYRy7vt+h/3HdOA6jILCtuv6wonAzeaaWPQ/Ced91lC11vND3D/f7dH6cDMe9UcKkNx4Ppqv5cn9713FDtUkitnu3e5a2PQPC3jKKqo20nMi1T7P5pNf848l8vwio5a0OJ/H8/fjpzXsywvGmE1TQBaG7WB6bsRUestof/KiCkjid7eCpDXwiwuF9pzz7bK/bXz5iEiXT2V2n4mds//TECfkkhFV8dhitq3YwGU9W83S2X29vqdxv1/sZm5hVFezNb+Oo9EjLXzxpHJ+AsLcu4WPwpsXvjKfpZmFRexIFlJpSsSgjtaltDahxdf3zYr+bFpU5Wd27XnFaW537J8zHxyOc+QV8JOjeruTPe8cZNSEh9QkVG5L/yLK9yLVu06k87sm83ymaZtvfPNpBPhbhKghku2CFRipFQqPVxqCWQ5eZ2kHUfdjLKMezwCvYHcd9rO94HMJhPyx28UnSzkF6oBbjUnJDqHc57SRtnD+4cg+R6O5xJudRCGe+/G6nu85jgsHMcC+JKgpiee4hzUFOToWJTh6nqo9AODFkB+j4+8xU9FIKr3bwSFwS1UBTF3jImfegaKyd4Pj/gXDfkdpFTUCG73jygwbdJIfxoCCT+TqskAUYyXWmkMN7WV+Iv714GC9FODxLjbI6W4EvSUnYrJxWv/q0ZNlR9ontPszFdwb9WPqOY106Gy9EmHbzt5G4L1zxeN9pGr5ahLTHauIL4nmpGK7jOZLe2p19QoTJSTKhjiXc33hddF/qKEqN0OypFJVLEM+EA0pj6ZneQZ/wXohwYOXvsbp70cQCtSF26O2V1gYRjjPBIZr4dRAJxYhf6t27+TOtaFpt3HMg3Eka6j2gg0j23YK1CxlvG3TqEcY0AAbp3sJYbItjHkicjWJM8e1HW9KT7vJTINy4+Ys74r1pWNBPB+fIUQERERp5N9k2H6FJgT046ergyjpp4GRI1tJgh/fPjjBZeHkv3+EATs+eUZBMfeZdDYRGAJou8zMSU9SDdST9xV0gCZhKsZXzoJsu10TYk8xHB2fgaOuX51uYmfLULX2kQkju+J/uZAe7gZ/LqmEJ8ylbOsvTTK7qIRzkc8PycJzmisxTnFOufVj6TIUQ3v4g/akD7SbFvvMM7Lo095/E17M3Wgin+WAJUz06xRV8huGyD3G5ZVtyAwqEzpr/ya586Vh+uNXZYF9L9rw7NzVEB+EqN6I+vmgVK52ez0zH5AG+cyrGeFWEVocP+VCCE4FZOVXpkWMAvUgW+Rztpma7aCCUrEYHe21d48M67MNpdwvf6hd6ARF6oRD3DG3eS19z+F/GqscTgWefT3Ffw2u0I9xllt+KYTaMz3U8JGAfr6IQLENylocCEU6PU5AjUr6xZJPQ28yUyVcjOiXlLnfbKVwrwl32OPsMU3ClZsuGsI1zz+hAQD6Sw6Ma1maepe/48Aav+mhogQO9Msm5eDvENoTzbASdPvSgiJ6c0Pf9osO3FvwndIS7YOfGEmVWIxwdJB21wY/P6xBSqgHTZGxl2uG2KWoLwlU2gt4C/oKe33OWk17Sm8wCSWOhhTv2DResvsTflAhXgazIMfSL7D1oSBwEUtLABWc8Omcd47eYm2aE0xwgWI8ePll67D43C+CuU4aZBL3SExBhkv0X7QtD4i6ZexxIE5NE59l8vjzFGe+JTvCYfOxbnEYjwkE24yLwXEPochLLOdp51iIwFMxO2JEPv6CztojQ7VDl7lrFz7AXYfauJb2NsfXJ3BAYBV87ZBOk2+j6mxCOvRLAKSLuFJPQSzFvAj4n9p7nS5lhwd8K/tADA3FfyDTFfGhH0hC60ot2go/bFqhHP4PoNhG4BoSJIdTfA4BiUnbKfSaIJQzCul/IDJuzsIrQ6A4BjYKS5hM7KEyxkSD/JAJEmaKSqCEmbkC4EA8IYA4KNyTs2ZYYa3iyMH7ASCqkf+1VEVoH/q+dxF5xwCTSE5YeJXy9mCUZROvuMQg3oi+dUwGgCx076NqEOC7nXT00NlEh2TdabXEW3QYVhEYEk+6QU3rAPM0xY+hhrlaC0AvnTKA3ci1zbi9HuBPTwe4XAIZgrsfgdPFD7Iw4z4P1douuZwsbwPhbCSHhLN0cZpa4SkkJYN76XneBM22OXyc+h5hTiqjW89chHAgrb535/BdmD6dkTzy6w9uJHiyb8atTB6yuCKfurArztqHbZ0JVIug5OXQGi/tgSSHiMhSKOoSOFt/t1iWLaxAmIsmHk1i4NdTYnHECKGw2DMt45mWr3sSGqZTcEYqwNx4O8/a7MGgYCqooKcwBMNXRAlq2xS8Qj79skvVIWGNtahCecA4TcAzCMaJSSlbM5f/GAebBk2nJvNyioVQyWKW3dBB9VoORf4Top6B4EEkVKakDjAxmm/MAPEH0vWXxn2fe2Hq4BGEqGuLzfhaO0bqDl+SRH6i/yCWF8MgCMbf7Bz+OAmW9hXu33g3NDesu1Nl5geQYXiK93kZTlBkIgLQXPwn2pkqUCIdi6CP+I2GyhMpts45GpRUshPB/lUKfUkKi+JkduNY9e3oINik3rfyRmAQYQo4BDeatUCB8e+bVKn66HqEIaOwF/ydGsiQCRdpk/Yy9KgJWAp2qSm80CQOFjmEgLKtN+MiiDgmqGqbSP6h4MADC6hFHtWyjQrgPRA/zXwjHGKClXIh5hmY263i0lrU57CYhfLlpg8PhLUYm9KMPDmgKM73L+7ifR4fc4WYBjK3KoioQTsQPOvzp2VR2xDrzNsIuA6W9F2oJc2mkRshKZr0oCsMwirzAqazu2/HdXPyUs9YdvAWZDngrm+mlnGkEQ1gyG60IhaXwuCUbSlka4VX3TBFJiEqbzUoglr2yltLJFrrWYr3czY/T6WQ6Pc53s3XfcuOgsERKPPylBfML2C5BJ7vkgP2hnInK1EyYfiOq6mkVoXDBwKIS2TLGmGkz045gFeYsB+SwabErVaB4/t1mPlElqEeT3UZdiwp+EJWPeNCTHIW1La1s8VE1E9HL9rYd4TDjRfzB24JljISiz7tIyHaSTtJQYEXkHnZib7NqqWtbrb2wnHjCds6g3ej4RlwVK8uNYH2OohnVNHEFYR8nSMhn3bykcoFIRayAVK9kkmXvF9JyrR3ae7312kmlchMsgCAWGDns1OmbLteltaA65zaEc5y0YBd7lbSa/SBr+qQYpEvmw+ncV3rz6/cfv2Hy8f3X5Y+Otx150RGdhyDWGL4ZSs8KmDKX4ZXTNmWEwhOgXa5SEdvI1W5YWX5B8eKZpJwJm3AU7xcvhHxHR/9us5OHeLz3JbYXgFHDCAZ1b6dO0wKXOYrYsVOa8yWEwsxEvCfSSPFAKxIRxDhW0xVPjs0ny3439Gyf/uiLq89AGMKJa3txoYoxWXp5MQDykyGUP0CasleXaefPEFTHXjch7OESDGjJuLJ6Bh+KMLD3oMpNO2GOb7oOYkgFMvP4bYbwP2K1wgpie5Opc7LM10ZjUALMrwPjPat7FPS0JzIifjFrU0QoCCZoxUJhyKHTRCy2qBbPdjdCS8YzOzcgLOz4TMjVL/KA2KG1FEH8aCtmvmXghN90HcsDD6mYNFxAp1O0ROhOlQiHaDiA/axqCSbJUpTbkh5HB5Edm566su2w6Z+uMoTfmgX6SpzurdDWyR021EZOSGdof430sI7Eg9UQlsgvmPACQpHc63INaSq+yxZ9NnI3WFmieHoolN3xhETyIhvEz8yyytluX7RLVBGJdZJMktouh3kqVh0x46NAKIaQc5M8vaAUVwRjS3l9H5Vt0o9L+mTdFhC+MKsql9cCD84wv62g6E9rbCmTmM8b8UhXdlQyQjGEIdOJOsMlJBL8SCRLiY+ge9tOZb7Y9LPXEsKkmNpGjN01zuE1mP5i/VOF8EoCCwIiyCgskUgIxRBC5npbZ2aEiNkvol/B63eq5WHmP36VEFIkS4UhtkOc4HPU1MDIooVxjSUFAfcmPIYrDb6EUHQqTx0oi36K4hySPGNj2WCje/1yhQI8kzb03xLCX+vW0OIFDKOojSChsaS8PRnMwubCuVButWxOc4RCLcFdK1bSK2Kfx2IERZpo7qt/x5LU7yWE/y4XCuUPjWHUxCqXQWjsxRYqWyoDIWslBrEzVCBEywLrI/XFZrJkBZfOQsyfmna4dGS+kRC+b5joWK+T9NVr3XVt4e0WaRCJ2OQI0bOB99QZwlwCSBGNDrXGjuV2v5cQfmPmFLgiHq42Ly6CCOZjgQ3vjioIxbTwR1JXaD4bAA6VKUMufF58JyH83mzw3zRGGT8CIp+JUzS4OTXOEGI6CTIR922GVBZM6k3rNNRAD5vRUiCmWdpJISQE19i/pB0AChcYiFFGKAIhnpsYV+vu6gXz4KpyxEw85gRygJ9d/WbWRrTQQIwtHi6ZLbzWReSUw2kJ4R5pRF/6h5ZgSvHYbbJ1bEh6EmnjpKbGmKJAliSp3YSpkIg7U0FrtyWEIi5kpjqpifvUjx0DwMYvdelXvi4gfE0NU7PLhaivNshWCKzGieFxRwWERzG2Zov6VNrBu7qFH/BXfywg/N2sy0pkAmnEVTv1yIRPMZFKw7oHgRA5GnDuO/0hBLI0bjAyTBgrlZ0FuosqMy0IifgozFSJhpr3rCWbKcgpIEzE8hjrhIm+YmC28txiD7ih+eVKRvhBQ1dwvawu7q0KeH3h93C9DhCu4I+QvGiw4pWmFzPOdcIzvDJAMKaDtjUch1uLppii3B7WlWJtINhJCFFJwaOoszPKlvOobNmqRozRvJaVFOIns/WHsTJp2/AirplITsWyOv9v5O28r1faeg8GedLqPPn3fi8h/NqUVgJrBcpuWpUkE66ZqJG46M4RYoUHKGlrYJgJ1DC1WEQDdeePEsKPZmPQjgJN6mnbU66FiSu9FxCiC+HcVd8ZgoPdtLMDrhoFQ4OmRsPZeYU0WqtAigbjBkiocYQYPfOlumMj05AEVnwn7d3L05lJcQg/48koU8NKQqTXGN/LwntzLviLQChWqT2z3UlJz5pqvpo72a/LCBmr0THbMCr1mc2ScAM6xm7n2z+M3IFAWKFLSSGBt9MwS5wDf1NB+F7TqkEB5kHTKVq3Ur/zaWfkw8Y56UDX3fO0a6LRQii5+U8F4T8kptH4e5sbQ91BdJPcsPBRMPJh42XkqeYYwhA251RBOJWqTkOYiO3+QqyXPWjORK4xgmZ3ACFuBwDDvNDUBl6SPWrho1y4avy7ipDl28oVQmrhzGmlaQF59ZgIW9gakpE5SJiGmp4HukNnCAlPLnyvQMjId03hRlGAarX7XXjfQRpxZneMTGm5f2yOSnMBZ6rjOoHvf15V0qsv2Ada+RK+kybV6E4mXWkiMjZlZNydh2OajwESrxVHcnX+VTENeeJbjyNyRdcabgMnIj6WZWuMbINjoN2lYmB04kjCa0QrviLzF7X7Y2QBDq3ZNieV2BIN9A3xDzCOmnaG95NWHAllo99eqRD+hzNGnXbzZV3NkAC4Gg4b9fmGsKycebQtOAnxtNvGyf5r1RCimmqlZqGf9BpHeIUkTj1vRxGmkqGZanYTT3boTFnQDKWSYnyhxVYg+6kZ9fCgaZ+lZQzBaDiH0zQ0fEOzFssI+TLL5yolFdZUyyXyoGClF2FwJGgFaQ8bIrnOs/maCQyZGTUKaEyFdWeD+FqXCfOVpZb0oxBOvlEbaQMMVDYSStrbIrA611T7KwQI1z9qEf7BPl7q8AYeqtWvdMjCLYrYt+knBnYMGGQ9Uso5xljDkoLbVHFSAdHUdXS80qlmb2lJILwQ+1/GBu4z5h5O06lyRa/fBpkLmMCPDQh/NzX1nTv9o5YhhAIiJHnR1ECF5Yo+1AtRXN0pCyVmv6ntDLc1v7Av9DQWgnhnaTozHq5hJsNbGWiggvmFfaRBaCCDrggrpEFkAYZOXgFm0QXuYiMSpAY6CL4pa67lLHhysL56JxMCayOV2LeAkMXBWdah6WGeNDAtwhcfsdTDmRn4/3g1no5Vw9xAa75arJfX8JkMIs9p6ibs9Jb9OKdEh2ivDRzNWP8BfNJreF8eGJofWhD+Aea0VecjqdktwukZNtC6N7YYO/W0aZEm+wFf2GsGiOl9De3xJD/eItybCZe/MG4BYUdfzbm+tJpSoDP13j5D+I2pRXK5MdUz9twvDDCWvzNgAwqJ9CkN36F22/ZVWLVpmYX5IM7bkgvcYfe0MoHchuf5dGRCtqYHMHC42wgUrCy2D6GYia29C1GKFjPlGaecc93x/4bwRIdqYme05bpht4bGEGKYmNUN1glkmPQQbgvs4Ay/P2vTUkiztUxD2FFEfeFVu4BPbEvcwWv1HfaopNDwe09nDKEzm7+K+yV+/e0LHfmNB1Et6cILtIznMcocG6KTlgJHEOBPzYaho3lKVUFalrGIzuTgwvfQJSWEUBQQPRPC+q3jjbJv9OcXICyOIc5DfS1v11KsJE8+15bfAGKTLb9ES7dFhOAPuaXRcoetXyVY5vFBx8ygsQGP0VRPB1qmxdrAlmaW5gEf0NaHuTgtX4Wtq/XZGaXHAGOzq/fol3iLoj9ETsODk76+x6930AGW535WH/hWhe8xMcvbHWXhyaGy+VALcJqM4CEX9dnz9XgpZ221KXax8V+x2tQ4iEBPzdryKj4wekE+p7A5L8XYgicT9WKL5kgNd2RdpKMcIuip8qi2vNlazLsQW/RFfAjNfnr01J08QkeZXKE9ravEvSAnX4oPMUMnR/5aD1C/S5yc3hL3Kgfxe/hpqh6nUD8C5hmZPMaX8zR6D2hIy0W4Mej95QBhEwaTtbIVsBqhl8XgowW47JmBeU9PP9cGy1cqhhfgVm+tkEIBEat6Tyoc3EfpVRnIMy5IRb40kOPiZgF2oQgQxb6SpgxpI8Iv8PeHqj2AlQS9nDz3CxiiB/Ms561vjCEjXFUYWxwQ84hJiBBxKibnCkQ+NXRWEsQIICUJpwbyNwihtag3pL0qyTYbD6+pWy3UgvgeIRpldVS/UymFbEA8NASHdmpUTyG8P8sVkfYZZ9HvtQCvst3qtVoMOXB2bnEJYld3QQ/Xb3v52pNA1WGP0Cvbg/ipyEwzgHUr2lS+/fDHNx8/fvzmjw/fsn+qv4eP6RGr7ZV1wtcPRdmMZRqCyPBD3jRrvjhvKyx1ZSqaXCnGh8L58P61KcuvHz8oQb5Ax2+OCkd8cJqiaSa4QqOLoD1jCMfh6S9fYY2f5BH5Zksu3yqKn15897upkt+/U2B88Qt+mjxIEHliS2dBT3xXlAxtKEKcvpz31bLCokA5TZ5XEWebmuYvlSa/ePG92JI9WC036+16n2aXVr7+voqRVw9ziP2MGUKSRbfYh313IdVioLu4xOFAQWWmpmF2KMx35fZevfgHTKzR/LYTRwG7EMkOIte/x4tIeh8qZkf4DOrTsmO8ePm23n6zAhCqsUbmIjh0zZ2VIeN4YvuNn2VlKvnfF7+BbRxu/dIdTnbk4u00X/9W+dUf4oHi9FdeGKpVtFHynPSHRlbQwtVXb30N9yHwKDg/X6EaEuJojO99hX4R28cbTqod8414JBwFCQdQaRaGFurarEJtIjMfmiVDEE6yx0inApQBXsEqvZl26+aPjWdGV4h6DnHKthrz6j9NG1GoaWAU2sjw8thfU9ex5NMmQX5fSBUg19BRv6nTQjii++vyZMwh9u4cyH3pFWJg1hCTLKyZRrZKA6kePVaD3C/9M98yXVa1Kwjax7Zd/mVB8ECfCk/I56K5/XMu2Y42gYHC8Q6hRlhsP+LDqxcEYzlXkt8UVmHbAHBYLrK9/ud18Q8WHOVVqUDNLaoJm580N5XwQFcc88q4HkOIrIZ/NtGsiSfFkyarpfj8KKhxOXFs//Xz3yWDgacxVuhs5hdBNIugC+PEuR5DiKE9FIdorV0YxfNDkgqTgbJDsxIi3Lx9+cNN6W94lFd1R8Z/JIC6+82AwB6kM1EZQjER+cqA7sYukh8C8/rzmsZVsp/X/3r18tVfZdgBHHT3ReUp3+aHWrVt4syexUygKPHjjkNeDeCV3pp+VRRWmgpDKCKEVSVgvfnh5cuXb8qDiIc2VaOSF58Juq7dKrmamMSmQJjVD5nVlcVaEXv6FVkn1NHKTnPrn68owlf/Lf8dj5Grhs7CpWrvVSpsMIQSPt7Mo3ymuPaRGHAQhQLg1ee8VdUtddc/coTvrssf4LE0qsCLJ+D2mjsR8ISTMN9tgQhFCRfP2OmaZXEOdLVKHXMR1VZdv33JpGJrhD5UqxghOTXUPuOBK6mIAaGWAB6N2494VY4urREJEcX0qbF+oKQqNRX3PlQfxTtRd+MaKilmKtAWAsI8Ir7AmorTI8t6inSkumnr+ktE+K/KR3ieTJn5wTRUHv2nFCCTqKROKiEU48bHdaq7i5TGiVwPSoQN2Ixit+31O0T4ZWUiwn6W8noO8Jqh/lEyfN6sCkoqzlTADgfjoX/aBi6lFRw+LrHIO6jI9c3NNSloqXV9cy3jhCPWzSJASGho75DFbDyaSsG6EKEUT+nvQTTEuZOJvNKEhFJanCLk3Zs3P/73xrh5wxH+dENu/v7xzZsvr/O2w1axgjq8AJNcv2haEe7hRcWxOIMHESbY43zG6+6cYQJL2q8lp4+WNF+pI+e3dOxevfrx2vqbewtKvt+8Yn96k48iHjovWdMrsDINC99lAYssgni8dyg7vQWdPkTT+rZGHEsvn8UGPCQv7bv+ieN69erNDR/En26Mt69elTwjmpp8ImL0pbFXPBOo90SClh1oJhCKbVp8el5yTBReLZBDhFpDaaH/moH54a93b1/9+BWdia/+e/Pm1Q9f/uttwTMC/8h3Lojw8pLjaXx5r2m0KiEUuy5BW/SPE8kOGfo1U1SziJAYr7hmXts//fz3V29e/fTVlz+/u7m+Ya7jbY4Qu/xFAeDokiOGoOkim2ObZYRidTSST8zSElxzeg2pz6svSghZxET18StCvvrph69ofGi/+vLGsL76iSlsrqV4yQhsNkXK3XosTEH4xBPq5ywrCE3ctQwmSDOZga0D8jDiacErzFnn6VsWMtFpeEfI9cu/yc837958ZVh/gc3JLwrAo8I5B3zxLdwKdXfJUV/wBGFCur0qQky5wfb3ygWELRBh7rGEsIhbpXWim3fMzFyzCP+v63c376g7JMzYvPwrt6XBMkeI0f3oIoBgQURVlXRyeY5QbHoAz3TRIBr2HcSqNP4RYygT+OvzX39zMJZlEYt7QWL99bd0WSPkMk22fHyFsVev5l7huibwIcyOCZ4oEIoBBq8yrTViJHSr93NaNlCkr4WfLp5bQsRNcNY/f0baTf8kd6KLh7tevfgc0uSD6iXYTlMMxfMzPcUBphJCMUnhVJY6c0rINJncVqgrgcp1M/mA3kJdRXxNbWkldjLyo9STKygYNlfVHRjOMq33YrB9RQxhLF2rI59BKwgdp251B5RRdRr0pGsfM+ngTHqPc1y1X4GFwIoAOL9I+DXG9HvFFhM6fea1597xcxfERCskAmWEgj8AN6hZDw4Hvf/p7qXr5DIRV0yKpyk6nBgvX71V+bhiUXFNntxfmsea2yUhjhMBWyhfc1E4Cxq9JeG7YEcKc0ocFnUFJNyrOsC2C0fpq87qsP759r8K9S/ejbhS3QjBJN7Q6amCWDwNiBTuKCsgFJrpwD2GlY4kd5QX2OxKlM5KtXeciDttuSg9jnWtaqF8B06yVRm5IGD3moT35rhyu4khTiAUqYC4cLlV8Ux24aZBaSqFAZQoUH95MmcB9ZrKMj/HkRRE+xBZ+da0eaQcwPluuV/fHqjnHd1VjSyn7cVrHWoQjgvWtmpsbPaYMKXYnc1Z2VgSL7IpNT502k7/Ze8KOv2MgAzUBy07O7mVlYdyM5OIPxePZC/fjSCYCKT3Kkk8b8dUodObhvXu2OqsswaP04eO13C2O7GjziHNrroeb2sOWmZHKaXdu8V2M0vnFeWB683EHsbyhUElhOKoM9w8WEkghKORZzn75uIpu7vJ7+fuzdfEj4IKTGI7oW/Il7SM1+qlVML6mfqiY9e2bScIyl8C0p5dhdcdFyGVb/AQR1sBsRuUM4vWwRyf/0wFqSOV1+GvO9vC7RzHdHvw/DgOI4/+J4xjPzis06l8bv7ktqN+FumvmFGL9/I1wLLA4Y3CaATlvR6Ve2bEN4GjzCrVa3SGjjGFTEJjVVcBYrsPu9JlIaPhZLpiMp2Myx+ld26d2lPCyW+zibbm2FHZ0UJDiW2WpIJQOGoS8K6pkDc72A0n7K9W/MD0P6jTV+J1b+eqW5DKMpqfurVPMXhZLr+RKFiYSTUggFxYZhTdyp1W1fueijO2V7VtQUhJseX2qQ9bDhtry6kdWaTN9wVN0n7Ha4lz4yVcSGsfBhXDgIuP2U1x1ds6FXd2if6Eo+CUJwRbndOE3WsVtZanWE4Y9merQfUurWQwn/XDsP4QcOokUHX9nblnPU0qBg7354ir4UineruUAuG0eKeV4pRbazs2e7MkedggNWt0e8RyorDj9bf7ZTqfr+bzdLnf9iM/9pzmNIwzG23gIglKodTV3x2ulCvBr8TVCi0IsxwsakD19FByGKw73rlnDrDF1cgAAAhsSURBVGG1NZpp5KXZ1XJBQG0p5V+UgGlE2CxmHBz4NKG0TpXihGvqs5mEtxm2I8wO0YYNPtWCXaou9HVWOIFIns5YzWKXy8Rbje8T+gqHVV4NJtVBxNPgs2PNY9UNaCqE+bGycMptr+ZcNuo5eMd6CTNlwXOC5M+iQWP4547dV28xG175EuqYuJBMYUdrEeZuEEj7RB2VUQ4XwYOpkgbpJSe5t4iVshdaC7O/NZOpOV7YimITLFPZCWvuVO/Nq0eYBSJ4J+1KuWrq9tgCEzXQs4Bndx51eaVK6ODxJfLAHJoTyo8S5RopnGg+FZ6Q1ECp+XNPGFALEig7VTTo8ekXJ2yjOQurGQ8gl+RwGxDyJT5qavZdQhVUcYEAgR1WWT0f8Wv2HtcgzK44FvZpqQhLyR31FfGKzXQaVqcsMCEPF6zqKMSKmTdmyVNWXxIdYQVftTcWyNoo+8hVOIpGhNn10IaH1y2qkhqE6Sg7z5uG1f+TUMMazU3Kd9pISj3A22ROzUqQJimb2zTirlt66oJVyWrBg00dkFqE+QWquF1rr+Rn4YClmWnIMetOBiEbysC6nx9cjdC3KITFVyyp2OtH3m7UZUkQ61RXNosAs0tM7H4tjnqEkpuBPOFMlT8hNmM1/iTpeHOzQ4cy4LNoTIOFC0Bann+3PkQ8V9sz0z/nPRqh0Wltq6t7CFCZfBAIUd0E3IpwlBUWurBgvFQZSzYRqFnfO/bePD9wnupErIR7ONO9Y8HrLnbMZNPYlBrENDWH07HrJxOX/lM5q/Ee9fxuaX9cD6MBoVQEgTfG7WoSsuGAsjerb56mCST7upPhZmDOWowOJaxewDK9dACmG2PH4upoZbK0zTB29nUnxBC8pTS/t099XbUGwtzXCIirmoTsmSlTx1xhnoeOxDKOz0ov7WAUG8b++bTfzQMjnCanOLLtgKXv2EwO3fk4pMG6+ngByxmWR7DOjLYjNOeZp3dhLk6q6yXwWoMfNYVlj3Qm0tYpGujE6+VywdJNzhQUaxCxnVhn2wk7cz7pwqMZk4hFuuoMlrgz8D4D6C5r29+O0MzXQmKwqL1zLTeLjmKtwq0JjEN4xpgyFJ8ao83C85mmOeaMzCaY8CR8LtfOYLE9J789M2w5hKMFoeTp8TrA5FRXg0UOWAVKNS3nqFaeW6FTa3rw49vRMKQDvuo6GGvzTWXDI17aHk9H9UVZeANhcsieH9U6Qk2Eko8Q3TerK+0j4hL2NF9as27ThZi8nnnsUvpjn89su87wf89byCpTCrO3XGog+dQi9WeTE3SD0uJptK5vuiZCCaKYAtPqCmlBgJFnaEUVur1hiTzbpUKwXOCejy85sAiIEU1O+WoBOmeYu5P8hsx2gBoIJTdoeWCne/3GGlRrJK0dBtYWq8g4B7O3SW88fiDUvCzXB9ElI76BLjo2HXlAXIyO5jmTC9tUVA+hmWZqSXAbj7lsvqJLNrjUQcD/oaCoCzytVlPzgf6v2Q8sYtlsLlI6xHuxblWN/1pQ601OrbLLUJ+KUHIaRowdOTjrFibnUA22dGx5nXvKqgkxN13X7q9T6his22Pbb+MFzJBRvikxv9D26Qjle/GyC2P31ftUW8TbUWLm+saA3QVMw0pIMbK5Z7VUXtviNIqptIzQaXT0FyI0h1EGxxJPHjzo36YFwto57gGocNCbzGf35xarxYS4tz3Rq1lPk0aqdjlCtoUse3Z8i0w+DS9MzXjndLWaWfxXYRx5TtOtyuJ1HsEM0/ghH2qbNJDtRyGUtqyyvBeuI4/WF6qqFVBYlxUjZaQslRYXxUWez4qw4OndLb5huHCfIzNTi68rllvHUn0GcTW8xCMQmkfJC2R3E1MiVr6i+vnw+bcivbTs5i8hzcHEExCaPWkiGGFfvP0TYbQ7t9lF7bJvcoyLTvS7CGHBmFGjuhGTYbpQ7WV+kjjdrQAyPsn32bjqxO9zITSnjgTFcbM7aQdrv2mV80IhUbQXaxCjjby8L658/nQIzeReytYQz87qQEapobpz/BHiuIe50I5k5koeiYQL1eLL8yJkezPkd3okr3WZbruPTpWK59lRuBfTz0yWoawZYnf7p0ZoJlu5cpCOY5o5p2R1ipv4czu8dU5VejNfJr/EPV08gI9EyGybV8AY7nOGkay2XuPadY1YgWttJCY22PoFbhDYl87ApyBkBKMwUo5/K5fLTZb9buhojyWxg7BzSgfSE+aHYvmJ3X3cmaiPR0gJW7FCywrtmUwVk2l6a7thoCollH9me6FrbIsVG5O9HxUf7t/q0tDnQ0gJ26mIkTi+VKPGpTdN1wenE4ceq520ML9IaOBrO44Xxb612Oymxdk1XJ7jIiEn8aG5ZKVRnoCQ9vWhRErpXDqkFcYxGh7ny/32tv9gWE7gWOfD4nY9S+fTYYU+T2bn8noHCe+O5a9dIk9CSE1Ov0y8KUh7v9KphSrLeL4Ow3LgYcV3jzMwmTwRIe31U7XizvY6D/vVJTNnOF8bfrVO03b7Txo/Jk9GSFu3VpSlEdtzncVsNWmL42ikv+9HsaoK1eneP2H+CXkGhJywKZMRluOFftRfL+fHybgnY016w8lqN9seXD8MlKVRVuTMHuPgK/IsCE1G2Nw6wsaKoaI4plgc48yEBLHvu2FDEoMEndMTp18mz4WQDuSu32mjMoRL83coPP8ufZbh4/J8CE22tt1XmIuLxPLch6q/eYo8K0KTlXXfR49hpRydE7qn3aPJS408N0Imk+WiE1dr1xvFdsLuYTbVzqDpy6dAyGSy2xpu6GlUWVp2EPnWffop0DH5VAiZjCa7/YJ04sgL7DJUTk2pifXt/iadPp9dqcqnRAiSDKerdLa+799JAO/69zXM9Nnl/wDZFx9trJOLygAAAABJRU5ErkJggg==",
      name: "PSG",
    },
  ];

  const handleSelectedPost =
    (post: Post): MouseEventHandler<HTMLIonCardElement> | undefined =>
    () => {
      setSelectedPost(post);
    };

  const renderList = (): JSX.Element[] => {
    return posts.map((post) => {
      return (
        <IonItem
          key={`card-${post.id}`}
          mode="ios"
          lines="none"
          className="ion-no-padding ion-no-inner-padding item"
        >
          <IonCard
            className="ion-no-padding card"
            onClick={handleSelectedPost(post)}
            >
            <motion.div className="card-content" layoutId={`card-${post.id}`}>
              <motion.div
                className="card-content"
                layoutId={`image-container-${post.id}`}
              >
                <IonImg className="card-image" src={post.image} />
                <IonCardContent>
                  <motion.div
                    className="title-container"
                    variants={{
                      show: {
                        opacity: 1,
                        transition: {
                          duration: 0.5,
                          delay: 0.3,
                        },
                      },
                      hidden: {
                        opacity: 0,
                        transition: {
                          duration: 0.1,
                        },
                      },
                    }}
                    initial="show"
                    animate={selectedPost?.id === post.id ? "hidden" : "show"}
                  >
                  </motion.div>
                </IonCardContent>
              </motion.div>
            </motion.div>
          </IonCard>
        </IonItem>
      );
    });
  };

  const renderSelectedPost = (currentPost: Post | undefined): JSX.Element => {
    if(!currentPost) return <></>
    return (
      <motion.div
        layoutId={`card-${currentPost?.id}`}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        className="popup-container"
        onClick={() => setSelectedPost(undefined)}
      >
        <IonImg src={currentPost?.image} />
        <motion.div
          initial={{ opacity: 0, transform: "translate(20px)" }}
          animate={{
            opacity: 1,
            transform: "translate(0)",
            transitionDuration: "0.5s",
            transitionDelay: "0.2s",
          }}
        >
          <h1>{currentPost?.name}</h1>
        </motion.div>
        <motion.div
          initial={{ opacity: 0, transform: "translate(20px)" }}
          animate={{
            opacity: 1,
            transform: "translate(0)",
            transitionDuration: "0.5s",
            transitionDelay: "0.25s",
          }}
        >
          <IonText>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. A
              consequuntur iusto officiis fugit nulla optio nesciunt delectus
              repellendus, sapiente dicta illum, deleniti consequatur beatae
              excepturi incidunt, vitae sit veritatis provident?
            </p>
          </IonText>
        </motion.div>
      </motion.div>
    );
  };

  return (
    <IonPage>
      <IonHeader mode="ios">
        <IonToolbar mode="ios">
          <IonTitle>Blog post</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div className="content-container">
          <IonList mode="ios" className="content-list ion-no-padding">
            {renderList()}
          </IonList>
          <AnimatePresence>
            {renderSelectedPost(selectedPost)}
          </AnimatePresence>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Home;
