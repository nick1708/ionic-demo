import {
  IonCardSubtitle,
  IonContent,
  IonList,
  IonSearchbar,
} from "@ionic/react";
import { KeyboardEvent, MouseEventHandler, useState } from "react";
import "../Employees/Employees.css";
import Layout from "../../components/Layout";
import { teamsList } from "../../data/teams";
import { AnimatePresence, motion } from "framer-motion";
import Card from "../../components/Card";
import Popup from "../../components/Popup/Popup";
import { ITeam } from "../../interfaces/ITeam";

const Teams = () => {
  const [teams, _setTeams] = useState(teamsList);
  const [selectedTeam, setSelectedTeam] = useState<ITeam | undefined>(
    undefined
  );

  const [results, setResults] = useState(teams);

  const search = (e: KeyboardEvent<HTMLIonSearchbarElement>) => {
    const searchTerm = e.currentTarget.value;

    if (searchTerm !== "") {
      const searchTermLower = searchTerm?.toLowerCase();

      const newResults = teams.filter((e) =>
        e.name.toLowerCase().includes(searchTermLower || "")
      );
      setResults(newResults);
    } else {
      setResults(teams);
    }
  };

  const handleSelectedTeams =
    (team: ITeam): MouseEventHandler<HTMLIonCardElement> | undefined =>
    () => {
      setSelectedTeam(team);
    };

  const renderTeamCards = (): JSX.Element[] => {
    return results.map((team) => {
      return (
        <motion.div
          key={`teamItem_${team.id}`}
          layoutId={`card-${team.id}`}
          variants={{
            show: {
              opacity: 1,
              transition: {
                duration: 0.3,
                delay: 0.2,
              },
            },
            hidden: {
              opacity: 0,
              transition: {
                duration: 0.1,
              },
            },
          }}
          initial="show"
          animate={selectedTeam?.id === team.id ? "hidden" : "show"}
        >
          <Card
            key={`teamItem_${team.id}`}
            handleCard={handleSelectedTeams(team)}
            id={`teamItem_${team.id}`}
            title={team.title}
            avatar={team.avatar}
            name={team.name}
          />
        </motion.div>
      );
    });
  };

  const renderTeamsEmployee = (currentTeam: ITeam | undefined): JSX.Element => {
    if (!currentTeam) return <></>;
    return (
      <Popup team={currentTeam} onClick={() => setSelectedTeam(undefined)} />
    );
  };

  return (
    <Layout>
      <IonCardSubtitle className="results">
        {results?.length} {results?.length === 1 ? "team" : "teams"} found
      </IonCardSubtitle>
      <IonSearchbar
        onKeyUp={(e) => search(e)}
        placeholder="Search..."
        slot="end"
      />
      <IonList mode="ios" className="content-list ion-no-padding">
        {renderTeamCards()}
      </IonList>
      <AnimatePresence>{renderTeamsEmployee(selectedTeam)}</AnimatePresence>
    </Layout>
  );
};

export default Teams;
