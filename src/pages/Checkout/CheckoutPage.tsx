import Layout from "../../components/Layout";

import CheckoutText from "../../components/Checkout/CheckoutText";
import CheckoutForm from "../../components/Checkout/CheckoutForm";
import CheckoutTitle from "../../components/Checkout/CheckoutTitle";
import CheckoutCart from "../../components/Checkout/CheckoutCart";

const CheckoutPage = () => {
  return (
    <Layout>
      <CheckoutTitle title="Checkout form" />
      <CheckoutText>
        Below is an example form built entirely with Bootstrap’s form controls.
        Each required form group has a validation state that can be triggered by
        attempting to submit the form without completing it.
      </CheckoutText>
      <CheckoutCart />
      <CheckoutForm />
    </Layout>
  );
};

export default CheckoutPage;
