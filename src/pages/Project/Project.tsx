import {
    IonText,
  } from "@ionic/react";
  
import { useHistory } from "react-router";
import Layout from "../../components/Layout";
import Image from "../../components/Image";
import Text from "../../components/Text";
import Title from "../../components/Title";
  
  const Project: React.FC = (): JSX.Element => {
  
    const {location: {state: Info } } = useHistory();

    return (
      <Layout>
        <Image src={Info?.project?.image || ''} />
        <Title  customClass="big orange" title={Info?.project?.name || ''} />
        <IonText color={"light"}>
          <Text customClass="white">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. A
            consequuntur iusto officiis fugit nulla optio nesciunt delectus
            repellendus, sapiente dicta illum, deleniti consequatur beatae
            excepturi incidunt, vitae sit veritatis provident?
          </Text>
        </IonText>
      </Layout>
    );
  };
  
  export default Project;
  