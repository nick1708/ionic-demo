import {
  IonCardSubtitle,
  IonList,
  IonSearchbar,
} from "@ionic/react";
import { KeyboardEvent, MouseEventHandler, useState } from "react";
import Card from "../../components/Card";
import Layout from "../../components/Layout";
import { employeeList } from "../../data/employees";
import { AnimatePresence, motion } from "framer-motion";

import Popup from "../../components/Popup/Popup";
import { IEmployee } from "../../interfaces/IEmployee";

const Home = () => {
  const [employees, _setEmployees] = useState<IEmployee[]>(employeeList);
  const [selectedEmployee, setSelectedEmployee] = useState<
    IEmployee | undefined
  >(undefined);

  const [results, setResults] = useState(employees);

  const search = (e: KeyboardEvent<HTMLIonSearchbarElement>) => {
    const searchTerm = e.currentTarget.value;

    if (searchTerm !== "") {
      const searchTermLower = searchTerm?.toLowerCase();

      const newResults = employees.filter((e) =>
        e.name.toLowerCase().includes(searchTermLower || "")
      );
      setResults(newResults);
    } else {
      setResults(employees);
    }
  };

  const handleSelectedEmployee =
    (employee: IEmployee): MouseEventHandler<HTMLIonCardElement> | undefined =>
    () => {
      setSelectedEmployee(employee);
    };

  const renderEmployeeCards = (): JSX.Element[] => {
    return results.map((employee) => {
      return (
        <motion.div
          key={`employeeItem_${employee.id}`}
          layoutId={`card-${employee.id}`}
          variants={{
            show: {
              opacity: 1,
              transition: {
                duration: 0.3,
                delay: 0.2,
              },
            },
            hidden: {
              opacity: 0,
              transition: {
                duration: 0.1,
              },
            },
          }}
          initial="show"
          animate={selectedEmployee?.id === employee.id ? "hidden" : "show"}
        >
          <Card
            key={`employeeItem_${employee.id}`}
            handleCard={handleSelectedEmployee(employee)}
            id={`employeeItem_${employee.id}`}
            title={employee.title}
            avatar={employee.avatar}
            name={employee.name}
            projectName={employee.projectName}
            developerType={employee.developerType}
          />
        </motion.div>
      );
    });
  };

  const renderSelectedEmployee = (
    currentEmployee: IEmployee | undefined
  ): JSX.Element => {
    if (!currentEmployee) return <></>;
    return (
      <Popup
        employee={currentEmployee}
        onClick={() => setSelectedEmployee(undefined)}
      />
    );
  };

  return (
    <Layout>
      <IonCardSubtitle className="results">
        {results?.length} {results?.length === 1 ? "employee" : "employees"}{" "}
        found
      </IonCardSubtitle>
      <IonSearchbar
        onKeyUp={(e) => search(e)}
        placeholder="Search..."
        slot="end"
      />
      <IonList mode="ios" className="content-list ion-no-padding">
        {renderEmployeeCards()}
      </IonList>
      <AnimatePresence>
        {renderSelectedEmployee(selectedEmployee)}
      </AnimatePresence>
    </Layout>
  );
};

export default Home;
