import { IProject } from "./IProject";

export interface IEmployee {
  id: string;
  name: string;
  description?: string;
  title: string;
  avatar: string;
  projectName: string;
  developerType: string;
  projectList: IProject[];
}