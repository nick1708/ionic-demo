export interface IProject {
    id: string;
    image?: string;
    name?: string;
}