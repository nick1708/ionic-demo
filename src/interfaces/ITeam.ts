import { ITechnologies } from "./ITechnologies";

export interface ITeam {
    id: string;
    name: string;
    description?: string;
    title: string;
    avatar: string;
    technologies?: ITechnologies[];
}