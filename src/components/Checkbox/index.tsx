import { IonCheckbox} from "@ionic/react";
import Label from "../Label";
import "./Checkbox.css";

interface ICheckbox {
  text?: string | undefined;
  customClass?: string | undefined;
  position?: "fixed" | "start" | "end" | "stacked" | undefined;
}

const Checkbox = ({
  text = "",
  customClass = "",
  position = "start",
}: ICheckbox): JSX.Element => {
  return (
    <IonCheckbox mode="md" className={`Checkbox ${customClass}`} labelPlacement={position}>
      <Label label={text} />
    </IonCheckbox>
  );
};

export default Checkbox;
