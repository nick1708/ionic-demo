
import "./CardImage.css";

interface CardImage {
  image: string;
}

const CardImage = ({ image }: CardImage): JSX.Element => {
  return (
    <div className="CardImage">
      <img src={image} alt="employee avatar" />
    </div>
  );
};

export default CardImage;
