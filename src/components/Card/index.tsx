import { IonItem } from "@ionic/react";
import CardHeader from "./CardHeader";
import CardBody from "./CardBody";
import "./Card.css";
import { MouseEventHandler } from "react";
import Text from "../Text";
import CardImage from "./CardImage";

interface CardEmplooyee {
  id: string;
  name: string;
  title: string;
  avatar: string;
  projectName?: string;
  developerType?: string;
  handleCard: MouseEventHandler<HTMLIonItemElement> | undefined;
}

const Card = ({
  id,
  name,
  title,
  avatar,
  projectName,
  developerType,
  handleCard,
}: CardEmplooyee): JSX.Element => {
  return (
    <IonItem
      onClick={handleCard}
      id={id}
      className={`Card animate__animated animate__fadeIn`}
      lines="none"
    >
      <CardImage image={avatar} />
      <div>
        <CardHeader name={name} />
        <CardBody>
          <Text>{title}</Text>
          <Text>{projectName}</Text>
          <Text>{developerType}</Text>
        </CardBody>
      </div>
    </IonItem>
  );
};

export default Card;
