import "./CardBody.css";

interface CardBody {
    children: JSX.Element[];
}

const CardBody = ({children}: CardBody): JSX.Element => {
  return (
    <div className="CardBody">
      {children}
    </div>
  );
};

export default CardBody;
