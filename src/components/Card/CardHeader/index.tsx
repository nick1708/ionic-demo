import "./CardHeader.css";
import Title from "../../Title";

interface CardHeader {
  name: string;
}

const CardHeader = ({ name }: CardHeader): JSX.Element => {
  return (
    <div className="CardHeader">
      <Title title={name} />
    </div>
  );
};

export default CardHeader;
