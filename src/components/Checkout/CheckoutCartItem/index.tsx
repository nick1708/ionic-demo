import { IonItem } from "@ionic/react";
import CheckoutCartTitle from "../CheckoutCart/CheckoutCartTitle";
import CheckoutCartText from "../CheckoutCart/CheckoutCartText/CheckoutText";

interface ICheckoutCartItem {
  title?: string | undefined;
  text?: string | undefined;
  price?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutCartItem = ({
  title = "",
  text = "",
  price = "",
  customClass = ""
}: ICheckoutCartItem): JSX.Element => {
  return (
    <IonItem className={`CheckoutCart-item ${customClass}`}>
      <div className="CheckoutCart-row">
        <CheckoutCartTitle title={title} customClass="CheckoutCartTitle" />
        <CheckoutCartText customClass="CheckoutCartText">
          {text}
        </CheckoutCartText>
      </div>
      <CheckoutCartText>{price}</CheckoutCartText>
    </IonItem>
  );
};

export default CheckoutCartItem;
