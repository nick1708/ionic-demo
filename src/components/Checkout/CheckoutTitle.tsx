import Title from "../Title";

interface CheckoutTitle {
  title?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutTitle = ({ title = "", customClass = "" }: CheckoutTitle): JSX.Element => {
    return <Title customClass={customClass} title={title}/>
  }

export default CheckoutTitle;
