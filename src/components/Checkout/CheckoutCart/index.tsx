import { IonContent, IonItem, IonLabel, IonList } from "@ionic/react";
import "./CheckoutCart.css";
import Title from "../../Title";
import CheckoutCartText from "./CheckoutCartText/CheckoutText";
import CheckoutTitle from "../CheckoutTitle";
import CheckoutCartItem from "../CheckoutCartItem";
import { cartList } from "../../../data/cart";

const CheckoutCart = (): JSX.Element => {
  const renderCartList = (): JSX.Element[] => {
    return cartList.map((cartItem) => {
      return (
        <CheckoutCartItem
          title={cartItem.title}
          text={cartItem.text}
          price={cartItem.price}
          customClass={cartItem.customClass}
        />
      );
    });
  };

  return (
    <>
      <CheckoutTitle customClass="CheckoutTitle" title="Your cart" />
      <IonContent className="CheckoutCart" color="light">
        <IonList className="CheckoutCart-list" inset={true}>
          {renderCartList()}
          <IonItem className="CheckoutCart-item">
            <Title title="Total (USD)" customClass="CheckoutCartTitle" />
            <CheckoutCartText>$20</CheckoutCartText>
          </IonItem>
        </IonList>
      </IonContent>
    </>
  );
};

export default CheckoutCart;
