import Title from "../../../Title";
import "./CheckoutCartTitle.css";

interface CheckoutCartTitle {
  title?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutCartTitle = ({
  title = "",
  customClass = "",
}: CheckoutCartTitle): JSX.Element => {
  return <Title title={title} customClass={customClass} />;
};

export default CheckoutCartTitle;
