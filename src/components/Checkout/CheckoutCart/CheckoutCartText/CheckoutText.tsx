import Text from "../../../Text";
import "./CheckoutCartText.css";


interface CheckoutCartText {
  children?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutCartText = ({ children, customClass = "" }: CheckoutCartText): JSX.Element => {
    return <Text customClass={customClass}>{children}</Text>
  }

export default CheckoutCartText;
