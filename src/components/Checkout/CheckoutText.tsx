import Text from "../Text";
import Title from "../Title";

interface CheckoutText {
  children?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutText = ({ children, customClass = "" }: CheckoutText): JSX.Element => {
    return <Text customClass={customClass}>{children}</Text>
  }

export default CheckoutText;
