import Input from "../../../Input";

interface ICheckoutFormInput {
  label?: string | undefined;
  customClass?: string | undefined;
  placeholder?: string | undefined;
}

const CheckoutFormInput = ({ label = "", customClass = "", placeholder = "" }: ICheckoutFormInput): JSX.Element => {
    return <Input label={label} customClass={customClass} placeholder={placeholder} />
  }

export default CheckoutFormInput;
