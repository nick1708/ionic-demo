import InputIcon from "../../../Input/InputIcon";

interface ICheckoutFormInputIcon {
  label?: string | undefined;
  icon?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutFormInputIcon = ({ label = "", customClass = "", icon = "" }: ICheckoutFormInputIcon): JSX.Element => {
    return <InputIcon label={label} customClass={customClass} icon={icon} />
  }

export default CheckoutFormInputIcon;
