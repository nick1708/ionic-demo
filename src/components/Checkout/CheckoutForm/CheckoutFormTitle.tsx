import Title from "../../Title";


interface CheckoutFormTitle {
  title?: string | undefined;
  customClass?: string | undefined;
}

const CheckoutFormTitle = ({ title = "", customClass = "" }: CheckoutFormTitle): JSX.Element => {
    return <Title customClass={customClass} title={title}/>
  }

export default CheckoutFormTitle;
