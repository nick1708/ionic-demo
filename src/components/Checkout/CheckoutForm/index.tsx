import CheckoutFormInput from "./CheckoutFormInput";
import CheckoutFormInputIcon from "./CheckoutFormInput/CheckoutFormInputIcon";
import CheckoutFormTitle from "./CheckoutFormTitle";
import { atCircleOutline } from "ionicons/icons";
import { countriesList } from "../../../data/countries";
import CheckoutFormSelect from "./CheckoutFormSelect";
import { statesList } from "../../../data/states";
import CheckoutFormCheckbox from "./CheckoutFormCheckbox";
import Button from "../../Button";
import InputRadio from "../../Input/InputRadio";
import { paymentList } from "../../../data/paymentType";
import "./CheckoutForm.css"

const CheckoutForm = (): JSX.Element => {
  return (
    <div className="CheckoutForm">
      <CheckoutFormTitle title="Billing address" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Name" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Last Name" />
      <CheckoutFormInputIcon label="User Name" icon={atCircleOutline} />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Email (Optional)" placeholder="you@example.com" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Address 2 (Optional)" placeholder="1234 Main St" />
      <CheckoutFormSelect label="Country" options={countriesList} />
      <CheckoutFormSelect label="State" options={statesList} />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Zip" />
      <div className="CheckoutForm-checkbox">
        <CheckoutFormCheckbox
          position="end"
          text="Shipping address is the same as my billing address"
        />
        <CheckoutFormCheckbox
          position="end"
          text="Save this information for next time"
        />
      </div>
      <CheckoutFormTitle title="Payment" />
      <InputRadio options={paymentList} labelPlacement="end" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Name on card" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Credit card number" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="Expiration" />
      <CheckoutFormInput customClass="CheckoutForm-input" label="CVV" />
      <Button text="Continue to checkout" />
    </div>
    
  );
};

export default CheckoutForm;
