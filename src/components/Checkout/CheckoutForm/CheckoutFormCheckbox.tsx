import Checkbox from "../../Checkbox";


interface ICheckoutFormCheckbox {
    text?: string | undefined;
    customClass?: string | undefined;
    position?: "fixed" | "start" | "end" | "stacked" | undefined;
}

const CheckoutFormCheckbox = ({ position = "start", text = "", customClass = "" }: ICheckoutFormCheckbox): JSX.Element => {
    return <Checkbox position={position} text={text} customClass={customClass} />
  }

export default CheckoutFormCheckbox;
