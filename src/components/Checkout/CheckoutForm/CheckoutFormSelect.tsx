import Select from "../../Select";

interface CheckoutFormSelect {
  label?: string | undefined;
  customClass?: string | undefined;
  options?: IOption[];
}

interface IOption {
    id?: string;
    label?: string;
    value?: string;
  }

const CheckoutFormSelect = ({ label = "", customClass = "", options = [] }: CheckoutFormSelect): JSX.Element => {
    return <Select options={options} label={label} customClass={customClass} />
  }

export default CheckoutFormSelect;
