import {
  IonItem,
  IonList,
  IonSelect,
  IonSelectOption,
} from "@ionic/react";
import Label from "../Label";

import "./Select.css";

interface ISelect {
  label?: string | undefined;
  customClass?: string | undefined;
  options?: IOption[];
}

interface IOption {
  id?: string;
  label?: string;
  value?: string;
}

const Select = ({ label = "Name", customClass = "", options = [] }: ISelect): JSX.Element => {

  const renderOptions = (): JSX.Element[] => {
    return options?.map((option) => {
      return <IonSelectOption key={option.id} value={option.value}>{option.label}</IonSelectOption>
    })
  }
  return (
    <div className={`Select ${customClass}`}>
      <Label label={label} customClass="Input-label" />
      <IonSelect
        className="Select-field"
        aria-label="Fruit"
        interface="action-sheet"
        placeholder={label}
      >
        {renderOptions()}
      </IonSelect>
    </div>
  );
};

export default Select;
