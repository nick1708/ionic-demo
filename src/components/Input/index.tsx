import { IonInput } from "@ionic/react";
import Label from "../Label";
import "./Input.css";

interface IInput {
  label?: string | undefined;
  customClass?: string | undefined;
  placeholder?: string | undefined;
}

const Input = ({ label = "", customClass = "", placeholder = "" }: IInput): JSX.Element => {
  return (
    <div className={`Input ${customClass}`} >
        <Label label={label} customClass="Input-label" />
        <IonInput placeholder={placeholder} className="Input-field" />
    </div>
  )
};

export default Input;
