import { IonRadio, IonRadioGroup } from "@ionic/react";
import "./InputRadio.css";


interface IInputRadio {
  customClass?: string | undefined;
  labelPlacement?: "fixed" | "end" | "start" | "stacked" | undefined;
  options: IInputRadioOption[]
}

interface IInputRadioOption {
  label?: string
  labelPlacement?: "fixed" | "end" | "start" | "stacked" | undefined;
  id?: string;
}

const InputRadio = ({ customClass = "", options = [], labelPlacement = "end"}: IInputRadio): JSX.Element => {

  const renderOptions = ():JSX.Element[] => {
    return options?.map((option) => {
      return <IonRadio justify="start" mode="md" alignment="start" className="InputRadio" key={option.id} value={option.labelPlacement} labelPlacement="end">{option.label}</IonRadio>
    })
  }
  return (
    <IonRadioGroup className="InputRadio-content" value={labelPlacement}>
      {renderOptions()}
  </IonRadioGroup>
  )
};

export default InputRadio;
