import { IonInput, IonItem, IonLabel } from "@ionic/react";
import { IonIcon } from "@ionic/react";
import Label from "../../Label";

interface IInputIcon {
  label?: string | undefined;
  icon?: string | undefined;
  customClass?: string | undefined;
  placeholder?: string | undefined;
}

const InputIcon = ({
  label = "Name",
  customClass = "",
  placeholder = "",
  icon = "",
}: IInputIcon): JSX.Element => {
  return (
    <div className={`Input ${customClass}`}>
      <Label label={label} customClass="Input-label" />
      <div className="Input-row">
        <IonIcon icon={icon} size="large" />
        <IonInput placeholder={placeholder} className="Input-field" />
      </div>
    </div>
  );
};

export default InputIcon;
