import { IonFooter,IonLabel } from "@ionic/react";
import "./Footer.css"

const Footer = (): JSX.Element => {
  return (
    <IonFooter class="Footer">
        <IonLabel class="Footer-text">
          <p>SMBS 2024.</p>
        </IonLabel>
      </IonFooter>
  );
};

export default Footer;