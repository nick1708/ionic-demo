import { IonText } from "@ionic/react";
import "./Text.css";
interface Text {
  children?: string | undefined;
  customClass?: string | undefined;
}

const Text = ({ children, customClass }: Text): JSX.Element => {
  return (
      <IonText className={`Text ${customClass}`}>
        <p>{children}</p>
      </IonText>
  );
};

export default Text;
