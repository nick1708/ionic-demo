import Employees from "../../pages/Employees/Employees";
import Navbar from "../Navbar";
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonMenu,
  IonMenuButton,
  IonNav,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";

const Header = (): JSX.Element => {
  return (
    <>
      <IonMenu contentId="main-content">
        <IonHeader>
          <IonToolbar>
            <IonTitle>Menu</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <IonButton className="NavItem-button" routerLink="/checkout">
            Checkout form
          </IonButton>
        </IonContent>
      </IonMenu>
    </>
  );
};

export default Header;
