import { IonImg } from "@ionic/react";
import "./Image.css"

interface Image {
  src?: string
}

const Image = ({ src }: Image): JSX.Element => {
  return <IonImg className="Image" src={src} />
};

export default Image;
