import { IonButton, IonButtons, IonIcon } from "@ionic/react";
import "./NavbarItem.css"

interface NavItem {
  slot: string;
  text: string;
  link: string;
  icon: string;
  iconRight: boolean;
}

const NavItem = ({slot, text, link, icon, iconRight}: NavItem): JSX.Element => {
    
  const renderNavItem = () => {
    if (!iconRight) {
      return (
        <IonButtons className="NavItem" slot={slot}>
        <IonButton className="NavItem-button" routerLink={link}>
            <IonIcon icon={icon} />
            {text}
          </IonButton>
        </IonButtons>
      );
    }

    return (
      <IonButtons className="NavItem" slot={slot}>
        <IonButton className="NavItem-button" routerLink={link}>
          {text}
          <IonIcon icon={icon} />
        </IonButton>
      </IonButtons>
    );
  };

  return renderNavItem() || <></>;
};

export default NavItem;
