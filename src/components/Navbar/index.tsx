import { IonToolbar } from "@ionic/react";
import { chevronBack, chevronForward } from "ionicons/icons";
import NavItem from "./NavbarItem";
import "./Navbar.css"

const Navbar = (): JSX.Element => {    
  return (
    <IonToolbar className="Navbar">
      <NavItem 
        slot="start"
        link="/home"
        icon={chevronBack}
        text="Employees"
        iconRight={false}
      />
      <NavItem 
        slot="end"
        link="/teams"
        icon={chevronForward}
        text="Teams"
        iconRight
      />
    </IonToolbar>
  );
};

export default Navbar;