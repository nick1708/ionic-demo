import { IonLabel } from "@ionic/react";
import "./Title.css";

interface Title {
  title?: string | undefined;
  customClass?: string | undefined;
}

const Title = ({ title, customClass }: Title): JSX.Element => {
  return <IonLabel className={`Title ${customClass}`}>{title}</IonLabel>;
};

export default Title;
