import { IonLabel } from "@ionic/react";
import "./Label.css"

interface ILabel {
  label?: string | undefined;
  customClass?: string | undefined;
}

const Label = ({ label = "Name", customClass = "" }: ILabel): JSX.Element => {
  return <IonLabel className={`Label ${customClass}`}>{label}</IonLabel>;
};

export default Label;
