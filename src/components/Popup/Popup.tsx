import { IonButton, IonImg, IonText } from "@ionic/react";
import { MouseEventHandler } from "react";
import { motion } from "framer-motion";
import { useHistory } from "react-router";
import "./Popup.css";
import { IEmployee } from "../../interfaces/IEmployee";
import { ITeam } from "../../interfaces/ITeam";
import Text from "../Text";
import Image from "../Image";
import Title from "../Title";
import Button from "../Button";

interface Popup {
  employee?: IEmployee;
  team?: ITeam;
  onClick: MouseEventHandler<HTMLDivElement>;
}

const Popup = ({ employee, team, onClick }: Popup): JSX.Element => {
  const history = useHistory();

  const renderImage = (): JSX.Element => {
    if (!team?.avatar) return <Image src={employee?.avatar} />;

    return <Image src={team?.avatar} />;
  };

  const renderTitle = (): JSX.Element => {
    if (!team?.name) return <Title customClass="big" title={employee?.name} />;

    return <Title customClass="big" title={team?.name} />;
  };

  const renderDeveloperType = (): JSX.Element => {
    if (!employee?.developerType) return <></>;

    return <Text>{employee.developerType}</Text>;
  };

  const renderDescription = (): JSX.Element => {
    if (!team?.description) return <Text>{employee?.description}</Text>;

    return <Text>{team.description}</Text>;
  };

  const renderSubtitle = (): JSX.Element => {
    if (!team?.technologies) return <p>Projects</p>;

    return <p>Technologies</p>;
  };

  const renderProjects = (employeeProjects: IEmployee): JSX.Element[] => {
    return employeeProjects.projectList.map((project) => {
      return (
        <Button
          text={project.name}
          onClickProps={() =>
            history.push({
              pathname: `/project/${project.id}`,
              state: { project },
            })
          }
        />
      );
    });
  };

  const renderTechnologies = (teamTechnologies: ITeam): JSX.Element[] => {
    if (!teamTechnologies.technologies) return [];

    return teamTechnologies.technologies.map((technologie) => {
      return <Button text={technologie.name} />;
    });
  };

  return (
    <motion.div
      layoutId={`poppu-${employee?.id}`}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className="Popup-container"
      onClick={onClick}
    >
      {renderImage()}
      <div className="Popup-content">
        <motion.div
          initial={{ opacity: 0, transform: "translate(20px)" }}
          animate={{
            opacity: 1,
            transform: "translate(0)",
            transitionDuration: "0.3s",
            transitionDelay: "0.2s",
          }}
        >
          {renderTitle()}
        </motion.div>
        <motion.div
          initial={{ opacity: 0, transform: "translate(20px)" }}
          animate={{
            opacity: 1,
            transform: "translate(0)",
            transitionDuration: "0.3s",
            transitionDelay: "0.25s",
          }}
        >
          {renderDeveloperType()}
          {renderDescription()}
          {renderSubtitle()}
          <IonText>
            <p>{employee && renderProjects(employee)}</p>
          </IonText>
          <IonText>
            <p>{team && renderTechnologies(team)}</p>
          </IonText>
        </motion.div>
      </div>
    </motion.div>
  );
};

export default Popup;
