import {
  IonButtons,
  IonContent,
  IonHeader,
  IonMenuButton,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import Header from "../Header";
import Footer from "../Footer";
import "./Layout.css";

interface Layout {
  children: JSX.Element[];
}

const Layout = (props: Layout): JSX.Element => {
  return (
    <>
      <Header />
      <IonPage className="Layout-page" id="main-content">
        <IonHeader>
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonTitle>Atomic Design</IonTitle>
          </IonToolbar>
        </IonHeader>
        {props.children}
      </IonPage>
    </>
  );
};

export default Layout;
