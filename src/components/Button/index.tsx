import { IonButton } from "@ionic/react";
import { MouseEventHandler } from "react";
import "./Button.css"

interface Button {
  text?: string;
  onClickProps?: MouseEventHandler<HTMLIonButtonElement>
}

const Button = ({ text, onClickProps }: Button): JSX.Element => {
  return <IonButton className="Button" onClick={onClickProps}>{text}</IonButton>
};

export default Button;
