import nickImage from "../assets/img/willy-trabajo.png"
import juanImage from "../assets/img/juan.jpg"
import robertImage from "../assets/img/robert.jpeg"
import josueImage from "../assets/img/josue.jpeg"
import luchoImage from "../assets/img/lucho.jpeg"
import smbsWebsite from "../assets/img/smart_business_solutions_smbs_logo.jpeg"
import aclaro from "../assets/img/aclaro.png"
import valeapp from "../assets/img/valelogo.png"

export const employeeList = [
    {
      id: "1",
      name: "Nick Rueda",
      description: 'As a frontend programmer, I am responsible for implementing and improving the user interface of our web applications, ensuring a smooth and engaging experience for our users.',
      title: "Web developer",
      projectName: "Aclaro",
      developerType: "Front-end developer",
      avatar: nickImage,
      projectList: [
        {
          id: "1",
          name:"Smbs Website",
          image: smbsWebsite
        },
        {
          id: "2",
          name:"ValeApp",
          image: valeapp
        },
        {
          id: "3",
          name:"Landings Pages",
          image: aclaro
        }
      ]
    },
    {
      id: "2",
      name: "Juan Query",
      description: 'I am a passionate backend developer with a strong background in software engineering and a proven track record in building and maintaining robust, scalable systems. With experience developing service-oriented applications and implementing efficient solutions, I am committed to technical excellence and delivering high-quality products.',
      title: "Web developer",
      projectName: "Traction Tools",
      developerType: "Back-end developer",
      avatar: juanImage,
      projectList: [
        {
          id: "1",
          name:"Smbs Website",
          image: smbsWebsite
        },
        {
          id: "2",
          name:"Traction Tools",
          image: smbsWebsite
        }
      ]
    },
    {
      id: "3",
      name: "Josue Lopez",
      description: 'I am a passionate mobile developer with proven experience in creating innovative and functional applications for iOS and Android platforms. My focus is on delivering exceptional user experiences and efficient and sustainable code development. With strong technical skills and a results-oriented mindset, I am committed to excellence in mobile software development.',
      title: "Mobile developer",
      projectName: "Gromming App",
      developerType: "Back-end developer",
      avatar: josueImage,
      projectList: [
        {
          id: "1",
          name:"Traction Tools",
          image: smbsWebsite
        },
        {
          id: "2",
          name:"Rentos",
          image: smbsWebsite
        },
        {
          id: "3",
          name:"Gromming App",
          image: smbsWebsite
        }
      ]
    },
    {
      id: "4",
      name: "Lucho Fajardo",
      description: 'I am a Fullstack developer passionate about creating comprehensive and efficient solutions. With experience in frontend and backend development, I specialize in creating complete web applications that deliver exceptional user experiences. My approach combines the versatility of working on all aspects of development, from user interface to database management, to ensure consistency and performance at all levels.',
      title: "Web developer",
      projectName: "PTAS",
      developerType: "Fullstack developer",
      avatar: luchoImage,
      projectList: [
        {
          id: "1",
          name:"PTAS",
          image: smbsWebsite
        },
        {
          id: "2",
          name:"Power Apps",
          image: smbsWebsite
        },
        {
          id: "3",
          name:"Node Trainee",
          image: smbsWebsite
        }
      ]
    },
    {
      id: "5",
      name: "Robert Samuel",
      description: 'I am a passionate programmer with extensive experience in software development and a strong commitment to teaching and mentoring. My goal is to impart solid knowledge and practical skills to other developers, inspiring them to reach their full potential. With exceptional communication skills and a strong technical foundation, I have proven effective in both software development and team training.',
      title: "Web developer",
      projectName: "Traction Tools",
      developerType: "Front-end developer",
      avatar: robertImage,
      projectList: [
        {
          id: "1",
          name:"Traction Tools",
          image: smbsWebsite
        },
        {
          id: "2",
          name:"Bootcamp Trainee",
          image: smbsWebsite
        }
      ]
    },
  ]