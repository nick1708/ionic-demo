export const cartList = [
    {
        id: '1',
        title: 'Product name',
        text: 'Brief description',
        price: '$12'
    },
    {
        id: '2',
        title: 'Second product',
        text: 'Brief description',
        price: '$8'
    },
    {
        id: '3',
        title: 'Third product',
        text: 'Brief description',
        price: '$5'
    },
    {
        id: '4',
        title: 'Promo code',
        text: 'EXAMPLECODE',
        price: '-$5',
        customClass: 'discount'
    },
]