import aclaroImage from "../assets/img/aclaro.png"
import bloomImage from "../assets/img/BloomGrowth.png"
import smbslogo from "../assets/img/smart_business_solutions_smbs_logo.jpeg"
import applaudologo from "../assets/img/applaudo_logo.jpeg"
import jmdlogo from "../assets/img/jmdlogo.png"
import baumlogo from "../assets/img/baum.jpeg"

export const teamsList = [
  {
    id: "1",
    name: "Aclaro",
    title: "IT marketing agency",
    description: 'We are A!claro, a marketing and IT consulting agency and we grow your company from a 720th perspective.',
    avatar: aclaroImage,
    technologies: [
      {
        id: "1",
        name: "Vue Js",
      },
      {
        id: "2",
        name: "HTML",
      },
      {
        id: "3",
        name: "CSS",
      },
      {
        id: "4",
        name: "Javascript",
      },
      {
        id: "5",
        name: "React Js",
      },
      {
        id: "6",
        name: "Sass",
      },
      {
        id: "7",
        name: "Wordpress",
      },
      {
        id: "8",
        name: "Php",
      },
    ]
  },
  {
    id: "2",
    name: "Bloom growth",
    description: 'Bloom is an online ecosystem of tools designed to empower business leaders and their teams to create new possibilities for their businesses and lives while nurturing organizational and team health.',
    title: "growth of your entire team",
    avatar: bloomImage,
    technologies: [
      {
        id: "1",
        name: ".Net",
      },
      {
        id: "2",
        name: "SQL",
      },
      {
        id: "3",
        name: "React Js",
      }
    ]
  },
  {
    id: "3",
    name: "Internal Projects",
    description: 'Smart Business Solutions is a full-service strategy digitalization company, whose job is to simply fall in love with our customers’ problems and to obsess over the solution, until our team can deliver above par the solutions. ',
    title: "Strategy digitalization company",
    avatar: smbslogo,
    technologies: [
      {
        id: "1",
        name: "React Js",
      },
      {
        id: "2",
        name: "Node Js",
      },
      {
        id: "3",
        name: "Next Js",
      },
      {
        id: "4",
        name: "Wordpress",
      },
      {
        id: "5",
        name: "Vue",
      },
      {
        id: "6",
        name: "Nest",
      },
      {
        id: "7",
        name: "Docker",
      },
      {
        id: "8",
        name: "Aws",
      },
      {
        id: "9",
        name: "Sass",
      },
      {
        id: "10",
        name: "Gatsby",
      }
    ]
  },
  {
    id: "4",
    name: "Applaudo Studios",
    description: `Our specialties include Digital Transformation, Web and Mobile Development, Cloud Computing, AI, and Machine Learning, among others. We are committed to delivering high-quality software solutions that not only are scalable and dependable but also future proof. We accelerate our customers digital roadmap by leveraging our 10 years of experience building custom digital solutions, augmenting our clients' teams, and reducing time-to-market.`,
    title: "T Services and IT Consulting",
    avatar: applaudologo,
    technologies: [
      {
        id: "1",
        name: "React Js",
      },
      {
        id: "2",
        name: "Next Js",
      },
      {
        id: "3",
        name: "AWS",
      },
      {
        id: "4",
        name: "Figma",
      },
      {
        id: "5",
        name: "PHP",
      }
    ]
  },
  {
    id: "5",
    name: "JMD Solutions",
    title: "CLOUD DEVELOPMENT",
    description: 'A group of talented Costa Rican programmers that had worked together for a few years, discovered the need to provide customers with quality and reusable code. Thus founded JMD Mobile Solutions understanding that quality applications require quality programming. Our core team of experts have under their wings more than 15 years of developing experience at corporate level and applications that are used by a huge amount of users everywhere at the same time.',
    avatar:jmdlogo,
      technologies: [
        {
          id: "1",
          name: "React Js",
        },
        {
          id: "2",
          name: "Power Apps",
        },
        {
          id: "3",
          name: ".Net",
        },
        {
          id: "4",
          name: "SQL",
        },
        {
          id: "5",
          name: "Dynamics 365",
        }
      ]
  },
  {
    id: "6",
    name: "Baum Digital",
    description: 'Baum Digital is a digital agency with over 9 years of experience, based in San Jose, Costa Rica. We build digitally-driven campaigns for both large and small companies in Costa Rica and regionally.',
    title: "Software Engineer",
    avatar:baumlogo,
    technologies: [
      {
        id: "1",
        name: "Wordpress",
      },
      {
        id: "2",
        name: "HTML",
      },
      {
        id: "3",
        name: "CSS",
      },
      {
        id: "4",
        name: "Javascript",
      },
      {
        id: "5",
        name: "PHP",
      }
    ]
  },
]